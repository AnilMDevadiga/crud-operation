<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>process</title>
<style type="text/css">
body{
color: white;
font-family: sans-serif;
background-color: rgb(51,51,51);
display: flex;
justify-content: space-around;
flex-wrap: wrap;
}
table{
display: table;
border-collapse: separate;
border-color: yellow;
border-spacing: 2px;
width: 100%;
}
td{
text-align: center;}
</style>
</head>
<body >
	
	<div>
		<h2> Employee ID : <span style="color: red;">${emp.id}</span> Details are Updated</h2>
		<hr>
		<table border="white" >
			  <tr>
			    <th>ID</th>
			    <th>Name</th>
			    <th>Department</th>
			  </tr>
			  <tr>
			    <td>${emp.id}</td>
			    <td>${emp.name}</td>
			    <td>${emp.dept}</td>
			  </tr>
		</table>
	</div>
	
</body>
</html>