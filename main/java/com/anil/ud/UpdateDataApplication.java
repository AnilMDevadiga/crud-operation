package com.anil.ud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpdateDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(UpdateDataApplication.class, args);
		
		System.out.println("Config file called");
	}

}
