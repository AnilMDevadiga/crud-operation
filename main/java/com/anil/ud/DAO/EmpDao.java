package com.anil.ud.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.anil.ud.DTO.EmpInfo;

public interface EmpDao extends JpaRepository<EmpInfo, Integer>
{

}
