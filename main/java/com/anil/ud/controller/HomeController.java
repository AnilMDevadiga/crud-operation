package com.anil.ud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.anil.ud.DAO.EmpDao;
import com.anil.ud.DTO.EmpInfo;

@Controller
public class HomeController 
{
	@Autowired
	EmpDao empdao;
	
	@RequestMapping("/")
	public String home()
	{
		System.out.println("home called...");
		return "home";
				
	}
	
	@RequestMapping("/process")
	public ModelAndView addInfo(ModelAndView mv, EmpInfo emp)
	{
		mv.addObject("emp", emp);
		mv.setViewName("process");
		empdao.save(emp);
		
		return mv;
	}
	
	@RequestMapping("/getinfo")
	public ModelAndView getInfo(@RequestParam int id,ModelAndView mv)
	{
		System.out.println("getinfo");
		EmpInfo emp = empdao.findById(id).orElse(new EmpInfo());
		//mv.addObject(emp);  //not working
		mv.addObject("emp", emp);
		mv.setViewName("getinfo");
		
		return mv;
	}
	
	@RequestMapping("/updateinfo")
	public ModelAndView updateInfo(ModelAndView mv, EmpInfo emp)
	{
		System.out.println("Update");
		
		empdao.save(emp);
		mv.addObject("emp", emp);
		mv.setViewName("update");
	
		return mv;
	}
	
	@RequestMapping("/delete")
	public ModelAndView deleteInfo(ModelAndView mv, EmpInfo emp)
	{
		System.out.println("delete");
		
		empdao.delete(emp);
		mv.addObject("emp", emp);
		mv.setViewName("delete");
	
		return mv;
	}
	
	
}
